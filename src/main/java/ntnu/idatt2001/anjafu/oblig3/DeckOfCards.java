package ntnu.idatt2001.anjafu.oblig3;

import java.util.ArrayList;
import java.util.Collections;

public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> deckOfCards = new ArrayList<>();

    public DeckOfCards() {
        for (int i = 0; i < suit.length; i++) {
            for (int j = 1; j <= 13; j++) {
                deckOfCards.add(new PlayingCard(suit[i], j));
            }
        }
    }

    public ArrayList<PlayingCard> getDeckOfCards() {
        return deckOfCards;
    }

    public HandOfCards dealHand(int n){
        HandOfCards handOfCards = new HandOfCards();
        //shuffle the cards so they are in random positions
        Collections.shuffle(deckOfCards);
        //since the cards are in random position, the first n cards will be random
        for (int i = 0; i < n; i++) {
            handOfCards.addCard(deckOfCards.get(i));
        }

        return handOfCards;
    }
}
