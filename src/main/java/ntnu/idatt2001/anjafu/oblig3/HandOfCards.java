package ntnu.idatt2001.anjafu.oblig3;

import java.util.ArrayList;

public class HandOfCards {
    ArrayList<PlayingCard> handOfCards;

    public HandOfCards() {
        handOfCards = new ArrayList<PlayingCard>();
    }

    public ArrayList<PlayingCard> getHandOfCards() {
        return handOfCards;
    }

    public boolean addCard(PlayingCard card){
        if(!(handOfCards.contains(card))){
            handOfCards.add(card);
            return true;
        }
        return false;
    }

    public boolean checkForFlush(int typeOfFlush){
        //first creates a new list with only a specific face of the cards and checks the size of the list with count
        //and whether that size is the same as the type of flush wanted
        if(handOfCards.stream().filter(c -> c.getSuit()=='S').count() >= typeOfFlush||
           handOfCards.stream().filter(c -> c.getSuit()=='H').count() >= typeOfFlush||
           handOfCards.stream().filter(c -> c.getSuit()=='D').count() >= typeOfFlush||
           handOfCards.stream().filter(c -> c.getSuit()=='C').count() >= typeOfFlush){
            return true;
        }
        return false;
    }
}
