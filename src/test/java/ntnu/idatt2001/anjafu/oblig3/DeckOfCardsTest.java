package ntnu.idatt2001.anjafu.oblig3;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
    @Test
    @DisplayName("Create a new deck of 52 cards")
    void createANewDeckOfCards(){
        DeckOfCards deckOfCards = new DeckOfCards();

        assertTrue(deckOfCards.getDeckOfCards().size() == 52);
        assertTrue(deckOfCards.getDeckOfCards().isEmpty() == false);
    }

    @Nested
    @DisplayName("Deal hand of cards")
    class dealHand{
        @Test
        @DisplayName("Deal hand of 5 cards")
        void dealFiveCards(){
            DeckOfCards deckOfCards = new DeckOfCards();

            HandOfCards handOfFiveCards = deckOfCards.dealHand(5);
            assertTrue(handOfFiveCards.getHandOfCards().size()==5);
            assertTrue(handOfFiveCards.getHandOfCards().isEmpty() == false);
        }

        @Test
        @DisplayName("Two hand of cards are different (and therefore random)")
        void twoDealHandsAreDifferent(){
            DeckOfCards deckOfCards = new DeckOfCards();

            HandOfCards handOfFiveCards1 = deckOfCards.dealHand(5);
            HandOfCards handOfFiveCards2 = deckOfCards.dealHand(5);

            assertTrue(!(handOfFiveCards1.getHandOfCards().equals(handOfFiveCards2.getHandOfCards())));
        }
    }


}