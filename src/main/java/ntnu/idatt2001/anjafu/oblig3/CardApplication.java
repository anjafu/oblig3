package ntnu.idatt2001.anjafu.oblig3;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class CardApplication extends Application {
    HandOfCards handOfCards;
    ArrayList<PlayingCard> handOfCardsList;
    String text = "";

    public static void main(String[] args) { launch(args); }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Card Application (oblig3, idatt2001)");

        DeckOfCards deckOfCards = new DeckOfCards();

        //creating the main layoutpane (gridpane)
        GridPane gp = new GridPane();
        gp.setMinSize(700,500);
        gp.setPadding(new Insets(60,60,60,60));
        gp.setHgap(13);
        gp.setVgap(13);

        //stackpane which will contain the cards
        StackPane sp = new StackPane();
        sp.setStyle("-fx-border-color: black");
        sp.setMinSize(400,300);

        //all the text
        Text cards = new Text("cards will show here");
        Text sumOfFaces = new Text("Sum of faces: ");
        Text sumOfFacesResult = new Text();
        Text cardsOfHearts = new Text("Cards of hearts: ");
        Text cardsOfHeartsResult = new Text();
        Text flush = new Text("Flush: ");
        Text flushResult = new Text();
        Text queenOfSpades = new Text("Queen of spades: ");
        Text queenOfSpadesResult = new Text();

        //some styling for the text
        sumOfFaces.setFont(Font.font("Tahoma", FontWeight.BOLD,12));
        cardsOfHearts.setFont(Font.font("Tahoma", FontWeight.BOLD,12));
        flush.setFont(Font.font("Tahoma", FontWeight.BOLD,12));
        queenOfSpades.setFont(Font.font("Tahoma", FontWeight.BOLD,12));

        //creating the buttons
        Button dealHand = new Button("Deal hand");
        Button checkHand = new Button("Check hand");
        dealHand.setPrefSize(100,20);
        checkHand.setPrefSize(100,20);

        //what happens when you click on dealhand button
        dealHand.setOnAction(actionEvent -> {
            text = "";
            handOfCards = deckOfCards.dealHand(5);
            handOfCardsList = handOfCards.getHandOfCards();
            handOfCardsList.forEach(c -> text += c.getAsString()+"   ");
            cards.setText(text);
        });

        //what happens when you click on checkhand button
        checkHand.setOnAction(actionEvent -> {
            //check for flush
            if(handOfCards.checkForFlush(5)==true){
               flushResult.setText("Yes");
            }else{flushResult.setText("No");}

            //find sum of faces
            sumOfFacesResult.setText(handOfCardsList.stream().map(PlayingCard::getFace).reduce((a,b) -> (a+b)).get().toString());

            //check for queen of spades
            if(handOfCardsList.stream().anyMatch(c -> c.getAsString().equals("S12"))){
                queenOfSpadesResult.setText("Yes");
            }else{queenOfSpadesResult.setText("No");}

            //find all heart cards
            text = "";
            handOfCardsList.stream().filter(c -> c.getSuit()=='H').forEach(c -> text += c.getAsString()+" ");
            if(!(text.isBlank())){
                cardsOfHeartsResult.setText(text);
            }else{cardsOfHeartsResult.setText("No hearts");}
        });

        //adding all the nodes to the gridpane
        sp.getChildren().add(cards);
        gp.add(sp, 1,1,6,6);
        gp.add(dealHand,12,2);
        gp.add(checkHand,12,5);
        gp.add(sumOfFaces,2,9);
        gp.add(sumOfFacesResult,3,9);
        gp.add(cardsOfHearts,5,9);
        gp.add(cardsOfHeartsResult,6,9);
        gp.add(flush,2,10);
        gp.add(flushResult,3,10);
        gp.add(queenOfSpades,5,10);
        gp.add(queenOfSpadesResult,6,10);

        //adding the gridpane to the scene and showing it
        primaryStage.setScene(new Scene(gp,700,500));
        primaryStage.show();
    }
}
