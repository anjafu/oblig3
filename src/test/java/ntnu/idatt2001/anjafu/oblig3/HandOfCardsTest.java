package ntnu.idatt2001.anjafu.oblig3;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    @Nested
    @DisplayName("Hand of cards does have flush")
    class HandOfCardsHasFlush{
        @DisplayName("Hand of 5 cards does have 5 flush")
        @Test
        void HandOf5CardsHas5Flush(){
            HandOfCards handOfCards = new HandOfCards();
            for (int i = 1; i <= 5; i++) {
                handOfCards.addCard(new PlayingCard('D',i));
            }
            assertTrue(handOfCards.checkForFlush(5));
        }

        @Test
        @DisplayName("Hand of 6 cards does have 5 flush")
        void HandOf6CardsHas5Flush(){
            HandOfCards handOfCards = new HandOfCards();
            for (int i = 1; i <= 6; i++) {
                handOfCards.addCard(new PlayingCard('H',i));
            }
            assertTrue(handOfCards.checkForFlush(5));
        }
    }


    @Nested
    @DisplayName("Hand of cards does not have 5 flush")
    class HandOfCardsHasNotFlush{
        @DisplayName("Hand of 5 cards does not have 5 flush")
        @Test
        void HandOf5CardsHasNot5Flush(){
            HandOfCards handOfCards = new HandOfCards();
            for (int i = 1; i <= 4; i++) {
                handOfCards.addCard(new PlayingCard('D',i));
            }
            handOfCards.addCard(new PlayingCard('H',1));

            assertFalse(handOfCards.checkForFlush(5));
        }

        @DisplayName("Hand of 6 cards does not have 5 flush")
        @Test
        void HandOf6CardsHasNot5Flush(){
            HandOfCards handOfCards = new HandOfCards();
            for (int i = 1; i <= 4; i++) {
                handOfCards.addCard(new PlayingCard('D',i));
            }
            handOfCards.addCard(new PlayingCard('H',1));
            handOfCards.addCard(new PlayingCard('S',3));

            assertFalse(handOfCards.checkForFlush(5));
        }
    }
}